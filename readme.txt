﻿=== Greenigma ===
Contributors: weblizar
Tags:two-columns, three-columns, custom-menu, right-sidebar, front-page-post-form, custom-background, featured-image-header, sticky-post, theme-options, threaded-comments, featured-images, flexible-header, translation-ready , blog , custom-logo , E-Commerce , footer-widgets , portfolio 
Requires at least: 4.0
Tested up to: 5.5
Stable tag: 2.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Free WordPress child theme can be used for different kinds of websites and multipurpose uses.

== Description ==
Greenigma is a Modern Material Design WordPress Theme. It is a child theme of most famous WordPress theme Enigma. It is intuitive and easy to use, robust and reliable, highly flexible and fully responsive theme. Based on live customizer, the theme allows to preview the changes as you go. It is the perfect theme for the professionals, bloggers, and creative personnel’s website, as it provides a clean and flexible appearance, an elegant portfolio, and a catchy online shop. The theme is translation ready, fully SEO optimized, fast loading and is fully compatible with woo commerce and all other major WordPress plugins.

== Frequently Asked Questions ==

= Where can i raise the theme issue? =

Please drop your issues here <a href="https://wordpress.org/support/theme/greenigma"> we'll try to triage issues reported on the theme forum, you'll get a faster response.

Screen shot URL :

Slider image URL : 
https://pxhere.com/en/photo/840048
All images are licensed under [CC0] (https://creativecommons.org/publicdomain/zero/1.0/)


== Changelog ==
=2.5=
***Update acoording to latest enigma

=2.4.1=
***Minor Bug Fixed.

=2.4=
***Competible with Enigma V6.0.5 and above.

=2.3=
***Competible with Enigma V6.0.1 and above.


=2.2=
***Bug Fixed.


=2.1=
***Bug Fixed.

= 2.0 =
* Footer widget option added.

= 1.8.9 =
* FullWidth with Row template added.
* Fontawesome issue fixed.
* minor corrections footer icons tooltip.

= 1.8.8 =
* fix header image

= 1.8.7 =
* added new post template(left sidebar)

= 1.8.6 =
* Tested with latest WP.
* Readme file changed as per new rule.

= 1.8.5 =
* Minor changes in header and footer
* box layout issue fixed
* Search box added
* Product Template added
* Screenshot update

= 1.8.4  =
* Logo issue fixed

= 1.8.3 =
* Header WP Title Issue Fixed