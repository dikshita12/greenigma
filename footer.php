<!-- Footer Widget Secton -->
<?php  if (is_active_sidebar('footer-widget-area')) { ?>
    <div class="enigma_footer_widget_area">
        <div class="container">
            <div class="row">
                <?php dynamic_sidebar('footer-widget-area'); ?>
            </div>
        </div>
    </div>
<?php } ?>
<div class="enigma_footer_area">
	<div class="container">
		<div class="col-md-12">
			<?php if ( enigma_theme_is_companion_active() ) { ?>
                <p class="enigma_footer_copyright_info wl_rtl">
                    <?php 
                    $enigma_footer_customization = get_theme_mod( 'enigma_footer_customization', __('&copy; Copyright 2020. All Rights Reserved','greenigma') );
                    echo esc_html( $enigma_footer_customization ); ?>
                    <a target="_blank" rel="nofollow" href="<?php echo esc_url( get_theme_mod( 'enigma_deve_link' ) ); ?>">
                       <?php 
                       $enigma_develop_by = get_theme_mod( 'enigma_develop_by' );
                       echo esc_html( $enigma_develop_by ); ?>
                    </a>
                </p>    
            <?php 
            }else{ ?>
                <p class="enigma_footer_copyright_info wl_rtl">
                    <?php esc_html_e('&copy; Copyright 2020. All Rights Reserved','greenigma'); ?>
                </p>    
            <?php
            }?>
			<?php if ( enigma_theme_is_companion_active() ) {
                $footer_section_social_media_enbled = absint(get_theme_mod('footer_section_social_media_enbled', 1));
                if ($footer_section_social_media_enbled == 1) { ?>
                    <div class="enigma_footer_social_div">
                        <ul class="social">
                            <?php
                            $fb_link = get_theme_mod('fb_link');
                            if (!empty ($fb_link)) { ?>
                                <li class="facebook" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e("Facebook",'greenigma') ?>"><a href="<?php echo esc_url(get_theme_mod('fb_link')); ?>"><i class="fab fa-facebook-f"></i></a></li>
                            <?php }
                            $twitter_link = get_theme_mod('twitter_link');
                            if (!empty ($twitter_link)) { ?>
                                <li class="twitter" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e("Twitter",'greenigma') ?>"><a href="<?php echo esc_url(get_theme_mod('twitter_link')); ?>"><i class="fab fa-twitter"></i></a></li>
                            <?php }
                            $linkedin_link = get_theme_mod('linkedin_link');
                            if (!empty ($linkedin_link)) { ?>
                                <li class="linkedin" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e("Linkedin",'greenigma') ?>"><a href="<?php echo esc_url(get_theme_mod('linkedin_link')); ?>"><i class="fab fa-linkedin-in"></i></a></li>
                            <?php }
                            $youtube_link = get_theme_mod('youtube_link');
                            if (!empty ($youtube_link)) { ?>
                                <li class="youtube" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e("Youtube",'greenigma') ?>"><a href="<?php echo esc_url(get_theme_mod('youtube_link')); ?>"><i class="fab fa-youtube"></i></a></li>
                            <?php }
                            $instagram = get_theme_mod('instagram');
                            if (!empty ($instagram)) { ?>
                                <li class="facebook" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e("instagram",'greenigma') ?>"><a href="<?php echo esc_url(get_theme_mod('instagram')); ?>"><i class="fab fa-instagram"></i></a></li>
                            <?php }
                            $vk_link = get_theme_mod('vk_link');
                            if (!empty ($vk_link)) { ?>
                                <li class="facebook" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e("vk",'greenigma') ?>"><a href="<?php echo esc_url(get_theme_mod('vk_link')); ?>"><i class="fab fa-vk"></i></a></li>
                            <?php }
                            $qq_link = get_theme_mod('qq_link');
                            if (!empty ($qq_link)) { ?>
                                <li class="facebook" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e("qq",'greenigma') ?>"><a href="<?php echo esc_url(get_theme_mod('qq_link')); ?>"><i class="fab fa-qq"></i></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } 
            } ?>
		</div>		
	</div>	
</div>	
<!-- /Footer Widget Secton -->
</div></div>
<a id="btn-to-top" ></a>
<?php wp_footer(); ?>
</body>
</html>